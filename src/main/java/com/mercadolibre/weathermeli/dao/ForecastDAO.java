package com.mercadolibre.weathermeli.dao;

import com.mercadolibre.weathermeli.domain.Forecast.Forecast;
import com.mercadolibre.weathermeli.entities.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ForecastDAO {

    @Autowired
    DataSource dataSource;

    public Weather findByDay(int day) {
        Weather result = null;
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("SELECT day, forecast FROM forecast WHERE day = ?");
            ps.setInt(1, day);
            ResultSet rs = ps.executeQuery();
            if(rs.next()) {
                result = new Weather();
                result.setDay(rs.getInt(1));
                result.setForecast(Forecast.valueOf(rs.getString(2).trim()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void save(Weather data) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO forecast(day, forecast) VALUES(?, ?)");
            ps.setInt(1, data.getDay());
            ps.setString(2, data.getForecast().name());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
