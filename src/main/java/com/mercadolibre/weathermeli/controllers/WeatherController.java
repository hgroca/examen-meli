package com.mercadolibre.weathermeli.controllers;

import com.mercadolibre.weathermeli.dao.ForecastDAO;
import com.mercadolibre.weathermeli.entities.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

    @Autowired
    ForecastDAO dao;

    @GetMapping("/api/galaxy/{galaxyName}/weather")
    public ResponseEntity<Weather> getWeather(@PathVariable String galaxyName, @RequestParam("day") Integer day) {
        ResponseEntity<Weather> result;
        if(!"FarGalaxy".equals(galaxyName)) {
            result = new ResponseEntity<>( HttpStatus.NOT_FOUND);
        } else {
            Weather weather = dao.findByDay(day % 360);
            if (weather == null) {
                result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                weather.setDay(day);
                result = new ResponseEntity<>(weather, HttpStatus.OK);
            }
        }

        return result;
    }

}
