package com.mercadolibre.weathermeli.controllers;

import com.mercadolibre.weathermeli.domain.Forecast.FarGalaxyForecaster;
import com.mercadolibre.weathermeli.domain.Forecast.Forecaster;
import com.mercadolibre.weathermeli.domain.Galaxy.Analyzer.FarGalaxyTriangleAnalyzer;
import com.mercadolibre.weathermeli.domain.Galaxy.FarGalaxy;
import com.mercadolibre.weathermeli.domain.WeatherTenYearsJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JobController {

    @Autowired
    WeatherTenYearsJob job;

    @RequestMapping("/job")
    public String job() {
        job.execute();
        return "job";
    }

}
