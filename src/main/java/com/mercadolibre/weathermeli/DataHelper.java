package com.mercadolibre.weathermeli;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class DataHelper {

    @Autowired
    DataSource dataSource;

    public void initDB() {
        try {
            Connection connection = dataSource.getConnection();
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("DROP TABLE forecast");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            Connection connection = dataSource.getConnection();
            Statement stmt = connection.createStatement();
            stmt.executeUpdate("CREATE TABLE forecast (id SERIAL PRIMARY KEY, day INTEGER, forecast char(10))");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
