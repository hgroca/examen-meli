
package com.mercadolibre.weathermeli;

import com.mercadolibre.weathermeli.domain.Forecast.FarGalaxyForecaster;
import com.mercadolibre.weathermeli.domain.Forecast.WeatherInformationSumary;
import com.mercadolibre.weathermeli.domain.Galaxy.Analyzer.FarGalaxyTriangleAnalyzer;
import com.mercadolibre.weathermeli.domain.Galaxy.FarGalaxy;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.sql.DataSource;
import java.sql.SQLException;

@Controller
@SpringBootApplication
public class WeatherApplication implements CommandLineRunner {

  public static final int YEAR_DAYS = 360;
  public static final int TEN_YEARS = 10;
  @Value("${spring.datasource.url}")
  private String dbUrl;


  public static void main(String[] args) throws Exception {
    SpringApplication.run(WeatherApplication.class, args);
  }

  @RequestMapping("/")
  String index() {
    return "index";
  }

  @RequestMapping("/test")
  String hello() {
    return "hello";
  }

  @Bean
  public DataSource dataSource() throws SQLException {
    if (dbUrl == null || dbUrl.isEmpty()) {
      return new HikariDataSource();
    } else {
      HikariConfig config = new HikariConfig();
      config.setJdbcUrl(dbUrl);
      return new HikariDataSource(config);
    }
  }

  @Override
  public void run(String... args) throws Exception {
      FarGalaxyTriangleAnalyzer farGalaxyTriangleAnalyzer = new FarGalaxyTriangleAnalyzer();
      FarGalaxy farGalaxy = new FarGalaxy();
      FarGalaxyForecaster farGalaxyForecaster = new FarGalaxyForecaster(farGalaxy, farGalaxyTriangleAnalyzer);
      WeatherInformationSumary result = farGalaxyForecaster.weatherInformationForRange(0, YEAR_DAYS * TEN_YEARS);
      System.out.println("** Resultado del ejercicio **");
      System.out.println(result.toString());
  }
}
