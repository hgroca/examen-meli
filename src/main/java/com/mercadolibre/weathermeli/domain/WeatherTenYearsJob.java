package com.mercadolibre.weathermeli.domain;

import com.mercadolibre.weathermeli.DataHelper;
import com.mercadolibre.weathermeli.dao.ForecastDAO;
import com.mercadolibre.weathermeli.domain.Forecast.FarGalaxyForecaster;
import com.mercadolibre.weathermeli.domain.Forecast.Forecaster;
import com.mercadolibre.weathermeli.domain.Forecast.WeatherInformationDay;
import com.mercadolibre.weathermeli.domain.Galaxy.Analyzer.FarGalaxyTriangleAnalyzer;
import com.mercadolibre.weathermeli.domain.Galaxy.FarGalaxy;
import com.mercadolibre.weathermeli.entities.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class WeatherTenYearsJob {

    @Autowired
    private ForecastDAO dao;

    @Autowired
    DataHelper dataHelper;


    public void execute() {
        FarGalaxy galaxy = new FarGalaxy();
        Forecaster forecaster = new FarGalaxyForecaster(galaxy, new FarGalaxyTriangleAnalyzer());
        WeatherInformationDay info;
        dataHelper.initDB();
        for (double day = 0; day< 360; day++) {
            info = forecaster.predict(day);
            Weather weather = new Weather();
            weather.setDay(info.getDay());
            weather.setForecast(info.getForecast());
            dao.save(weather);
        }
    }

}
