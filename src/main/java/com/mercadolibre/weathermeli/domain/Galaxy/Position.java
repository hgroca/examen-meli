package com.mercadolibre.weathermeli.domain.Galaxy;

public class Position {
    //To compare two doubles we must define epsilon
    public static final double epsilon = 0.000001;
    public static final Position ZERO = new Position(0.0,0.0);

    private double x;
    private double y;

    public Position(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("x: %.10f, y: %.10f", x, y);
    }

    @Override
    public boolean equals(Object obj) {
        Position other = (Position) obj;
        return (Math.abs(x - other.getX()) < epsilon) && (Math.abs(y - other.getY()) < epsilon);
    }
}
