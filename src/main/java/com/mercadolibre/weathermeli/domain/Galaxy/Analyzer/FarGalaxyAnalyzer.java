package com.mercadolibre.weathermeli.domain.Galaxy.Analyzer;


import com.mercadolibre.weathermeli.domain.Galaxy.Galaxy;

public interface FarGalaxyAnalyzer {
    boolean arePlanetsAlignedToSun();
    boolean arePlanetsAligned();
    boolean isSunBetweenPlanets();
    double rainIntensity();
    void setTime(double time);
    void setGalaxy(Galaxy galaxy);
}
