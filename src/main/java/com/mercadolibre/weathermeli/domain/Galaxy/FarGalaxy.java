package com.mercadolibre.weathermeli.domain.Galaxy;

import org.springframework.stereotype.Component;

@Component
public class FarGalaxy extends Galaxy {

    public static final int PLANET_FERENGI = 0;
    public static final int PLANET_BETASOIDE = 1;
    public static final int PLANET_VULCANO = 2;

    public FarGalaxy() {
        super();
        planets = new Planet[3];
        planets[PLANET_FERENGI] = new Planet(1, 500);
        planets[PLANET_BETASOIDE] = new Planet(3, 2000);
        planets[PLANET_VULCANO] = new Planet(-5, 1000);
    }
}
