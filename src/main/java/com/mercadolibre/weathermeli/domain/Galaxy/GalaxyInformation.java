package com.mercadolibre.weathermeli.domain.Galaxy;

public class GalaxyInformation {

    private double time;
    private boolean planetsAligned;
    private boolean sunAligned;
    private boolean sunBetweenPlanets;
    private double perimeter;
    private double surface;
}
