package com.mercadolibre.weathermeli.domain.Galaxy;

import com.mercadolibre.weathermeli.domain.Forecast.Forecast;

public class GalaxyPlayer {
    private Galaxy galaxy;
    protected double time;
    protected double step;
    protected Forecast forecast;


    public void setTime(double time) {
        this.time = time;
        for (Planet planet : galaxy.getPlanets()) {
            planet.moveToTime(time);
        }
        event();
    }

    public void nexStep() {
        setTime(time += step);
    }

    public void prevStep() {
        setTime(time -= step);
    }

    public void event() {
        Forecast newForecast = Forecast.GREAT;
        if(!newForecast.equals(forecast)) {
            System.out.println(String.format("Cambio de clima de %s a %s", forecast.toString(), newForecast.toString()));
            forecast = newForecast;
        }
    }

}

