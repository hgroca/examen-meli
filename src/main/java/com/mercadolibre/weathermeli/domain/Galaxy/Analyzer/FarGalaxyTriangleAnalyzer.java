package com.mercadolibre.weathermeli.domain.Galaxy.Analyzer;

import com.mercadolibre.weathermeli.domain.Galaxy.Galaxy;
import com.mercadolibre.weathermeli.domain.Galaxy.Position;
import org.springframework.stereotype.Component;

@Component
public class FarGalaxyTriangleAnalyzer implements FarGalaxyAnalyzer {

    public static final double delta = 10;
    public static final double epsilonPlanetsAligned = 60000;
    private Galaxy galaxy;
    private double time;

    public Galaxy getGalaxy() {
        return galaxy;
    }

    @Override
    public void setGalaxy(Galaxy galaxy) {
        this.galaxy = galaxy;
    }

    protected double calculateSurface(Position positionPlanet1, Position positionPlanet2, Position positionPlanet3) {
        Position vector1 = new Position(positionPlanet2.getX() - positionPlanet1.getX(), positionPlanet2.getY() - positionPlanet1.getY());
        Position vector2 = new Position(positionPlanet3.getX() - positionPlanet1.getX(), positionPlanet3.getY() - positionPlanet1.getY());
        return Math.abs(((vector1.getX()* vector2.getY()) - (vector2.getX() * vector1.getY()))/ 2);
    }


    @Override
    public boolean arePlanetsAlignedToSun() {
        double planetsTriangleSurface = calculateSurface(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(1).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time));
        double sunTriangleSurface = calculateSurface(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(1).positionForTime(time), new Position(0.0,0.0));
        double sunTriangleSurface1 = calculateSurface(galaxy.getPlanets().get(2).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time), new Position(0.0,0.0));
        return (Math.abs(planetsTriangleSurface - sunTriangleSurface) < delta) && (Math.abs(planetsTriangleSurface - sunTriangleSurface1) < delta);
    }

    @Override
    public boolean arePlanetsAligned() {
        double planetsTriangleSurface = calculateSurface(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(1).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time));
        return Math.abs(planetsTriangleSurface) < epsilonPlanetsAligned;
    }


    @Override
    public boolean isSunBetweenPlanets() {
        Double planetsTriangleSurface = calculateSurface(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(1).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time));
        Double sunAndPlanets01TriangleSurface = calculateSurface(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(1).positionForTime(time), new Position(0.0,0.0));
        Double sunAndPlanets12TriangleSurface = calculateSurface(galaxy.getPlanets().get(1).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time), new Position(0.0,0.0));
        Double sunAndPlanets02TriangleSurface = calculateSurface(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time), new Position(0.0,0.0));
        return Math.abs(planetsTriangleSurface - (sunAndPlanets01TriangleSurface + sunAndPlanets12TriangleSurface + sunAndPlanets02TriangleSurface)) < delta;
    }

    public double rainIntensity() {
        double distanceP0P1 = calculateDistance(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(1).positionForTime(time));
        double distanceP0P2 = calculateDistance(galaxy.getPlanets().get(0).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time));
        double distanceP1P2 = calculateDistance(galaxy.getPlanets().get(1).positionForTime(time), galaxy.getPlanets().get(2).positionForTime(time));
        return distanceP0P1 + distanceP0P2 + distanceP1P2;
    }

    protected double calculateDistance(Position positionA, Position positionB) {
        return Math.sqrt(Math.pow(positionB.getX() - positionA.getX(), 2) + Math.pow(positionB.getY() - positionA.getY(), 2));
    }


    @Override
    public void setTime(double time) {
        this.time = time;
    }
}
