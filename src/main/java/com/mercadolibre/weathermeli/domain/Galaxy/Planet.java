package com.mercadolibre.weathermeli.domain.Galaxy;

public class Planet {
    //degrees / day
    private int speed;
    private int distance;
    private Position currentPosition;

    public Planet(int speed, int distance) {
        this.speed = speed;
        this.distance = distance;
        currentPosition = Position.ZERO;
        moveToTime(0);
    }

    public Planet(int speed, int distance, Position initialPosition) {
        this.speed = speed;
        this.distance = distance;
        currentPosition = initialPosition;
    }


    public void moveToTime(double time) {
        currentPosition.setX(Math.cos(Math.toRadians(getAngleForDayFraction(time))) * distance);
        currentPosition.setY(Math.sin(Math.toRadians(getAngleForDayFraction(time))) * distance );
    }

    public Position positionForTime(double time) {
        Position result = new Position(Math.cos(Math.toRadians(getAngleForDayFraction(time))) * distance  ,
                Math.sin(Math.toRadians(getAngleForDayFraction(time))) * distance );
        return result;
    }

    public double getAngleForDayFraction(double fraction) {
        return (speed * fraction) % 360;
    }
}
