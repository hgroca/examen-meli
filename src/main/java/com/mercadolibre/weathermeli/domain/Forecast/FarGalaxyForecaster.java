package com.mercadolibre.weathermeli.domain.Forecast;

import com.mercadolibre.weathermeli.domain.Galaxy.Analyzer.FarGalaxyAnalyzer;
import com.mercadolibre.weathermeli.domain.Galaxy.FarGalaxy;
import com.mercadolibre.weathermeli.domain.Galaxy.Galaxy;
import org.springframework.stereotype.Component;

@Component
public class FarGalaxyForecaster implements Forecaster{

    Galaxy galaxy;
    FarGalaxyAnalyzer analyzer;

    public FarGalaxyForecaster(FarGalaxy farGalaxy, FarGalaxyAnalyzer analyzer) {
        this.galaxy = farGalaxy;
        this.analyzer = analyzer;
        analyzer.setGalaxy(galaxy);
    }

    @Override
    public WeatherInformationSumary weatherInformationForRange(double from, double to) {
        double maxRainIntensity = 0;
        double maxRainDay = 0;
        double rainIntensity = 0;
        WeatherInformationSumary result = new WeatherInformationSumary();
        for(double day=from; day < to; day++) {
            WeatherInformationDay weather = predict(day);
            switch(weather.getForecast()) {
                case DROUGHT: result.setDroughtDays(result.getDroughtDays() + 1); break;
                case GREAT: result.setGreatDays(result.getGreatDays() + 1); break;
                case RAIN: result.setRainDays(result.getRainDays() + 1); break;
            }

            rainIntensity = weather.getRainIntensity();
            if (rainIntensity > maxRainIntensity) {
                maxRainIntensity = rainIntensity;
                maxRainDay = day;
            }
        }
        result.setMaxRainDay((int)maxRainDay);
        result.setFrom((int)from);
        result.setTo((int)to);
        return result;
    }

    public WeatherInformationDay predict(double day) {
        WeatherInformationDay result = new WeatherInformationDay();
        analyzer.setTime(day);
        Forecast forecast;
        forecast = getForecast();
        result.setForecast(forecast);
        result.setTime(day);
        result.setRainIntensity(analyzer.rainIntensity());
        return result;
    }

    private Forecast getForecast() {
        Forecast forecast;
        if(analyzer.arePlanetsAlignedToSun()) {
            forecast = Forecast.DROUGHT;
        } else if(analyzer.arePlanetsAligned()) {
            forecast = Forecast.GREAT;
        } else if(analyzer.isSunBetweenPlanets()) {
            forecast =  Forecast.RAIN;
        } else {
            //Planets are NOT aligned and Sun is NOT Between Planets
            forecast = Forecast.CLEAR;
        }
        return forecast;
    }
}
