package com.mercadolibre.weathermeli.domain.Forecast;

public class WeatherInformationDay {
    private double time;
    private Forecast forecast;
    private double rainIntensity;

    public double getTime() {
        return time;
    }

    public int getDay() {
        return (int) time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public Forecast getForecast() {
        return forecast;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }

    public double getRainIntensity() {
        return rainIntensity;
    }

    public void setRainIntensity(double rainIntensity) {
        this.rainIntensity = rainIntensity;
    }
}
