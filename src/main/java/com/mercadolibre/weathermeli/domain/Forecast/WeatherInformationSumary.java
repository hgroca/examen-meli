package com.mercadolibre.weathermeli.domain.Forecast;

import java.io.StringWriter;

public class WeatherInformationSumary {
    private int from;
    private int to;
    private int droughtDays;
    private int rainDays;
    private int greatDays;
    private int maxRainDay;


    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getDroughtDays() {
        return droughtDays;
    }

    public void setDroughtDays(int droughtDays) {
        this.droughtDays = droughtDays;
    }

    public int getRainDays() {
        return rainDays;
    }

    public void setRainDays(int rainDays) {
        this.rainDays = rainDays;
    }

    public int getGreatDays() {
        return greatDays;
    }

    public void setGreatDays(int greatDays) {
        this.greatDays = greatDays;
    }

    public int getMaxRainDay() {
        return maxRainDay;
    }

    public void setMaxRainDay(int maxRainDay) {
        this.maxRainDay = maxRainDay;
    }

    @Override
    public String toString() {
        StringWriter sw = new StringWriter();
        sw.append(String.format("Informacion del periodo %05d-%05d\n", from, to));
        sw.append(String.format("\tdroughtDays (Sequia): %04d\n", droughtDays));
        sw.append(String.format("\trainDays (Lluvia): %04d\n", rainDays));
        sw.append(String.format("\tgreatDays (Optimos): %04d\n", greatDays));
        sw.append(String.format("\tmaxRainDay (Dia de mayor lluvia): %04d", maxRainDay));
        return sw.toString();
    }
}
