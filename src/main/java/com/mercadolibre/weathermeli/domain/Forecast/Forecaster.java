package com.mercadolibre.weathermeli.domain.Forecast;

public interface Forecaster {
    WeatherInformationSumary weatherInformationForRange(double from, double to);
    WeatherInformationDay predict(double day);
}
