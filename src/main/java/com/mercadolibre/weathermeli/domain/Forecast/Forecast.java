package com.mercadolibre.weathermeli.domain.Forecast;

public enum Forecast {
    DROUGHT,
    RAIN,
    CLEAR,
    GREAT;
}