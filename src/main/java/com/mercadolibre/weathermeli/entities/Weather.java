package com.mercadolibre.weathermeli.entities;

import com.mercadolibre.weathermeli.domain.Forecast.Forecast;

public class Weather {
    private Integer day;
    private Forecast forecast;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Forecast getForecast() {
        return forecast;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }

}
